import {all, fork} from 'redux-saga/effects';

import authFetch from './contact/saga';

export default function* rootSaga() {
  yield all([authFetch()]);
}
