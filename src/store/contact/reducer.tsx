/**
 * @format
 * @flow
 */

import {

  SET_CONTACT
} from './constant';

const initialState = {
  data: []
};

export default (state = initialState, action) => {
  switch (action.type) {

    case SET_CONTACT:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};
