/**
 * @format
 * @flow
 */

import {
  SET_CONTACT 
} from './constant';

export const setContact = form => 
  ({
    type: SET_CONTACT,
    payload: form,
  })

  // export const setCoord = form => 
  // ({
  //   type: SET_ADDRESS,
  //   payload: form,
  // })

