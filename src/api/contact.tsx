import http from './http';

export const allContact = data => {
  return http.get('/contact');
};

export const addContact = data =>{
  console.log('addContact', data)
  return http.post('/contact', data);
}

export const deleteContact = data =>{
  console.log('idContact', data)
  return http.delete('/contact/'+data)
}

export const getContact = data =>{
  console.log('idContact', data)
  return http.get('/contact/'+data)
}

export const updateContact = data =>{
  const contact = {
    firstName: data.firstName,
    lastName: data.lastName,
    age: data.age,
    photo: data.photo
  };
  console.log('data', data.id, contact)
  return http.put('/contact/'+data.id, contact)
}