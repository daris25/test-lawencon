import { StyleSheet } from 'react-native';

export default StyleSheet.create({
center:{
    alignItems: 'center',
    justifyContent: 'center'
},

    container: {
        flex: 1,
        // backgroundColor: 'maroon'
    },
    scrollVoew:{
        // backgroundColor: 'lightgrey',
        height: '100%',
        width: '100%'
    },  

    headerForm:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: 50,
        backgroundColor: 'white',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.5,
        padding:10
    },
        textHead:{
            fontSize: 20,
            fontWeight: 'bold'
        },  
        imgAsset:{
            width: 35,
            height: 35
        },

    boxLetter:{
        width: '100%',
        height: 45,
        // backgroundColor: 'grey',
        // marginTop: 10,
        padding: 10
    },

    fontLetter:{
        fontSize: 16,
        fontWeight: 'bold',
        color: 'grey'
    },
    
    boxCard:{
        width: '100%',
        // height: 50,
        // backgroundColor: 'white',
        // borderRadius: 5,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.5,
        padding: 10,
        paddingLeft: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    imgCard:{
        width: 40,
        height: 40,
        borderRadius: 100
    },
    boxRightCard:{
        marginLeft: 20
    },
    textName:{
        fontSize: 15,
        fontWeight: 'bold',
        color: 'black'
    },
    textAge:{
        fontSize: 15,
        color: 'grey'
    }

})