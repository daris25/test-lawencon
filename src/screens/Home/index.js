import React, { useEffect } from 'react';
import {SafeAreaView, View, Text, TouchableOpacity, ScrollView, Image, Alert, ToastAndroid} from 'react-native';
import styles from './style'
import {allContact, deleteContact} from '../../api/contact'

import {setContact} from '../../store/contact/action'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux/lib/hooks/useSelector';

function Home(props) {
    const data = useSelector(state=> state.contact.data)
    console.log('dataselector', data)

    useEffect(()=>{
        pushAPI()
    },[0])

    const dispatch = useDispatch()
    async function pushAPI(params) {
        const all = await allContact()
        // console.log('all', all.status)
        if (all.status == 200) {
            // console.log('true')
            dispatch(setContact(all.data.data))
        } else {
            // console.log('false')
        }
    }

    const dellContact = async (id)=>{
        // console.log('idContact', id)
        try {
            const dell = await deleteContact(id)
            console.log('resDell', dell)
        } catch (error) {
            console.log('errorDelete', error.message)
            ToastAndroid.show(error.message, ToastAndroid.LONG);
        }
    }

    const alertDel = (params)=>{
        Alert.alert(
            params.firstName+' '+params.lastName,
            "You sure want to delete "+ params.firstName+' '+params.lastName,
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => dellContact(params.id)}
            ],
            { cancelable: false }
          );
    }

    return(
        <SafeAreaView style={styles.container}>
            <View
                style={{width: '100%', height: 50, marginTop: -50, backgroundColor: 'white'}}
            />
            <View style={{...styles.headerForm}}>
                <View />
                <View>
                    <Text style={{...styles.textHead}}>Contact</Text>
                </View>
                <TouchableOpacity
                    onPress={()=> props.navigation.navigate('Add')}
                >
                    <Image 
                        source={require('../../assets/plus.png')}
                        style={{...styles.imgAsset}}
                    />
                </TouchableOpacity>
            </View>         
            <ScrollView 
                style={{...styles.scrollVoew}}
            >
                <View>
                    {
                        data !== '' ?
                        data.map((person,i)=>{
                            return(
                                <TouchableOpacity 
                                    onPress={()=> props.navigation.navigate('Add', {id: person.id})}
                                    key={i} style={{...styles.boxCard}}
                                >
                                    <View style={{flexDirection: 'row'}}>
                                        <Image 
                                            source={{uri: person.photo}}
                                            style={{...styles.imgCard}}
                                        />
                                        <View style={{...styles.boxRightCard}}>
                                            <Text style={{...styles.textName}}>{person.firstName} {person.lastName}</Text>
                                            <Text style={{...styles.textAge}}>age {person.age}</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity
                                        onPress={()=> alertDel(person)}
                                    >
                                        <Image
                                            source={require('../../assets/delete.png')}
                                            style={{width: 40, height: 40, marginRight: 10}}
                                        />
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            )
                        })
                        : null
                    }
                    
                </View>
            </ScrollView>
        </SafeAreaView>
    )

}

export default Home;