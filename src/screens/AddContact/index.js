import React, {useEffect, useRef, useState } from 'react';
import {SafeAreaView, View, Text, TouchableOpacity, ScrollView, TextInput, Image, ToastAndroid, ActivityIndicator} from 'react-native';
import styles from './style'
import img from '../../config/images'
import {addContact, getContact, updateContact} from '../.././api/contact'

function AddContact(props) {
    console.log('props addCOntact', props.route.params)
    const ref_input2 = useRef()
    const ref_input3 = useRef()
    const ref_input4 = useRef()

    const [first, setFirst] = useState('')
    const [last, setLast] = useState('')
    const [age, setAge] = useState('')
    const [url, setUrl] = useState(img.avatar)
    const [loading, setLoading] = useState(false)
    
    useEffect(()=>{
        props.route.params !== undefined ? 
            pushAPI()
        : null
    },[])

    const pushAPI = async ()=>{
        setLoading(true)
       try {
            const get = await getContact(props.route.params.id)
            setFirst(get.data.data.firstName)
            setLast(get.data.data.lastName)
            setAge(get.data.data.age.toString())
            setUrl(get.data.data.photo)
            setLoading(false)
            console.log('getRes', get.data.data)
       } catch (error) {
            console.log('error', error.message)
       }
    }

    const changeUrl = (params)=>{
        if (params == '') {
            setUrl(img.avatar)
        } else {
            setUrl(params)
        }
    }

    const saveContact = async ()=>{
        if (first == '') {
            ToastAndroid.show('Fist name is empty .. ', ToastAndroid.LONG)
        } else if (last == '') {
            ToastAndroid.show('Last name is empty .. ', ToastAndroid.LONG)
         }else if (age == '') {
            ToastAndroid.show('Age is empty .. ', ToastAndroid.LONG)
        } else {
            setLoading(true)
            // console.log('data is ready to save')
            if (props.route.params == undefined) {
                console.log('save Add')
                try {
                    const data ={
                        firstName: first,
                        lastName: last,
                        age: age,
                        photo: url
                    }
                    const add = await addContact(data)
                    console.log('save addd', add)
                    if(add.status == 201){
                        setLoading(false)
                        props.navigation.reset({
                            index: 0,
                            routes: [{name: 'Home'}]
                        })
                    }
                } catch (error) {
                    console.log('error', error.message)
                    setLoading(false)
                    ToastAndroid.show(error.message, ToastAndroid.LONG)
                }
            } else {
                console.log('save Update')
                try {
                    const update = await updateContact({
                        id: props.route.params.id,
                        firstName: first,
                        lastName: last,
                        age: age,
                        photo: url
                    })
                    console.log('resUpdate', update)
                    if(update.status == 201){
                        setLoading(false)
                        props.navigation.reset({
                            index: 0,
                            routes: [{name: 'Home'}]
                        })
                    }
                } catch (error) {
                    console.log('errroe', error.message)
                    setLoading(false)
                    ToastAndroid.show(error.message, ToastAndroid.LONG)
                }
            }
        }
    }

    return(
        <SafeAreaView style={styles.container}>
            <View
                style={{width: '100%', height: 50, marginTop: -50, backgroundColor: 'white'}}
            />
            <View style={{...styles.headerForm}}>
                <TouchableOpacity
                    onPress={()=> props.navigation.goBack()}
                >
                    <Image
                        source={require('../../assets/arrow.png')}
                        style={{width: 25, height: 25}}
                    />
                </TouchableOpacity>
                <View>
                    <Text style={{...styles.textHead}}>{props.route.params !== undefined ? 'Update Contact' : 'Add Contact'}</Text>
                </View>
                <View style={{width: 25, height: 25}}/>
            </View>         
            <ScrollView>
                <View style={{...styles.boxHead}}>
                    <Image 
                        source={{uri: url}}
                        style={{...styles.imgHead}}
                    />
                    <Text style={{...styles.textHead, marginTop: 5}}>{first} {last}</Text>
                    <Text style={{...styles.textHead, color: 'grey'}}>{age == '' ? age : age+' years old'}</Text>
                </View>
                {
                    loading == false ?
                    <View>
                        <View style={{...styles.boxInput}}>
                            <Image
                                source={require('../../assets/peson.png')}
                                style={{...styles.imgIcon}}
                            />
                            <View style={{...styles.cardRight}}>
                                <Text style={{...styles.textHeadInput}}>First Name</Text>
                                <TextInput
                                    placeholder="Input first name"
                                    style={{...styles.textInput}}
                                    autoFocus={true}
                                    blurOnSubmit={false}
                                    autoCorrect={true}
                                    onSubmitEditing={() => ref_input2.current.focus()}
                                    value={first}
                                    onChangeText={(value)=> setFirst(value)}
                                />
                            </View> 
                        </View>

                        <View style={{...styles.boxInput}}>
                            <Image
                                source={require('../../assets/peson.png')}
                                style={{...styles.imgIcon}}
                            />
                            <View style={{...styles.cardRight}}>
                                <Text style={{...styles.textHeadInput}}>Last Name</Text>
                                <TextInput
                                    placeholder="Input first name"
                                    style={{...styles.textInput}}
                                    ref={ref_input2}
                                    onSubmitEditing={() => ref_input3.current.focus()}
                                    value={last}
                                    onChangeText={(value)=> setLast(value)}
                                />
                            </View> 
                        </View>

                        <View style={{...styles.boxInput}}>
                            <Image
                                source={require('../../assets/peson.png')}
                                style={{...styles.imgIcon}}
                            />
                            <View style={{...styles.cardRight}}>
                                <Text style={{...styles.textHeadInput}}>Age</Text>
                                <TextInput
                                    placeholder="Input first name"
                                    style={{...styles.textInput}}
                                    ref={ref_input3}
                                    onSubmitEditing={() => ref_input4.current.focus()}
                                    keyboardType="numeric"
                                    value={age}
                                    onChangeText={(value)=> setAge(value)}
                                />
                            </View> 
                        </View>

                        <View style={{...styles.boxInput}}>
                            <Image
                                source={require('../../assets/image.png')}
                                style={{...styles.imgIcon, height: 30}}
                            />
                            <View style={{...styles.cardRight}}>
                                <Text style={{...styles.textHeadInput}}>Link Photo (optional)</Text>
                                <TextInput
                                    placeholder="Input link photo"
                                    style={{...styles.textInput, height: null}}
                                    ref={ref_input4}
                                    multiline={true}
                                    value={url == img.avatar ? '' : url}
                                    onChangeText={(value)=> changeUrl(value)}
                                />
                            </View> 
                        </View>
                            {/* <Text>** photo is optional if empty is displayed the default image</Text> */}
                        <TouchableOpacity
                            onPress={()=> saveContact()}
                            style={{...styles.btnSave}}
                        >
                            <Text>SAVE</Text>
                        </TouchableOpacity>
                    </View>
                    : <ActivityIndicator size="large" color="black" />
                }
            </ScrollView>
        </SafeAreaView>
    )

}

export default AddContact;