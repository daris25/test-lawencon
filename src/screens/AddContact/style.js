import { Dimensions, StyleSheet } from 'react-native';

const width = Dimensions.get('screen').width
console.log('width', width - (width/100*20))

export default StyleSheet.create({
center:{
    alignItems: 'center',
    justifyContent: 'center'
},

    container: {
        flex: 1,
        // backgroundColor: 'maroon'
    },

    boxInput:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        // height: 50,
        // backgroundColor: 'maroon',
        padding: 10,
        borderBottomColor: 'grey',
        borderBottomWidth: 0.5
    },

    imgIcon:{
        width: 40,
        height: 40,
        resizeMode: 'stretch'
    },

    cardRight:{
        marginLeft: 10,
        // backgroundColor: 'maroon'
    },
        textHeadInput:{
            color: 'grey',
            fontSize: 13,
            // backgroundColor: 'grey'
        },  
        textInput:{
            fontSize: 15,
            // backgroundColor: 'yellow',
            height: 45,
            width: width - (width/100*18),
            marginTop: -10,
            marginBottom: -10,
            flexWrap: 'wrap'
        },
        btnSave:{
            width: '90%', 
            height: 50,
            backgroundColor: 'tomato',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            marginTop: 20,
            borderRadius: 5
        },

    boxHead:{
        padding: 15,
        // justifyContent: 'center',
        alignItems: 'center',
        // height: 200,
        marginTop: 50,
        marginBottom: 10
        // backgroundColor: 'maroon'
    },
        imgHead:{
            width: 110,
            height: 110,
            borderRadius: 100
        },
        textHead:{
            fontSize: 15,
            fontWeight: 'bold'
        },

    headerForm:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: 50,
        backgroundColor: 'white',
        borderBottomColor: 'lightgrey',
        borderBottomWidth: 0.5,
        padding:10
    },
        textHead:{
            fontSize: 20,
            fontWeight: 'bold'
        }
})